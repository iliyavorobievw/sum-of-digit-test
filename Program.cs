﻿using System;
using System.Linq;

namespace Sum_of_Digit___test
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            //493193  -->  4 + 9 + 3 + 1 + 9 + 3 = 29  -->  2 + 9 = 11  -->  1 + 1 = 2
        }
    }
    public class Number
    {
        public int DigitalRoot(long n)
        {

            while (n >= 10)
            {
                n = n.ToString().ToCharArray().Select(c => Convert.ToInt32(c.ToString())).Sum();
            }

            return (int)n;

            // Your awesome code here!
        }
    }
}
